class Tile {
  int player = 0;
  int dots = 0;

  Tile();

  Tile.copy(Tile t) {
    player = t.player;
    dots = t.dots;
  }

  Tile.fromJSON(Map<String, dynamic> json)
      : player = json['player'],
        dots = json['dots'];

  Map<String, dynamic> toJson() => {
        'player': player,
        'dots': dots,
      };
}
