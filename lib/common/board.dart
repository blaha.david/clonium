import 'tile.dart';

class Board {
  final int boardSize;
  final int numberOfPlayers;
  int playerPlaying = 0; // 0=none, 1=player1, 2=player2, 3=player3, 4=player4
  List<Tile> tiles = [];

  Board(this.boardSize, this.numberOfPlayers) {
    tiles = List.generate(boardSize * boardSize, (index) => Tile());
    var x1 = boardSize - 2;
    var y1 = 1;
    var x2 = 1;
    var y2 = boardSize - 2;
    tiles[y1 * boardSize + x1].player = 1;
    tiles[y2 * boardSize + x2].player = 2;
    tiles[y1 * boardSize + x1].dots = 3;
    tiles[y2 * boardSize + x2].dots = 3;

    playerPlaying = 1;
  }

  bool validMove(int x, int y) {
    if (x < 0 || x >= boardSize || y < 0 || y >= boardSize) return false;
    return tiles[y * boardSize + x].player == playerPlaying;
  }

  void play(int x, int y) {
    int index = y * boardSize + x;
    tiles[index].dots += 1;
    var foundFour = false;
    do {
      foundFour = false;
      for (int n = 0; n < tiles.length; n++) {
        if (tiles[n].dots >= 4) {
          int ni = n % boardSize;
          int nj = n ~/ boardSize;
          tiles[n].dots -= 4;

          if (tiles[n].dots == 0) {
            tiles[n].player = 0;
          }

          if (ni != 0) {
            tiles[n - 1].dots += 1;
            tiles[n - 1].player = playerPlaying;
          }
          if (ni != boardSize - 1) {
            tiles[n + 1].dots += 1;
            tiles[n + 1].player = playerPlaying;
          }
          if (nj != 0) {
            tiles[n - boardSize].dots += 1;
            tiles[n - boardSize].player = playerPlaying;
          }
          if (nj != boardSize - 1) {
            tiles[n + boardSize].dots += 1;
            tiles[n + boardSize].player = playerPlaying;
          }
          foundFour = true;
        }
      }
    } while (foundFour);

    playerPlaying++;
    if (playerPlaying > numberOfPlayers) {
      playerPlaying = 1;
    }
  }

  Board.fromJSON(Map<String, dynamic> json)
      : boardSize = json['board_size'] as int,
        numberOfPlayers = json['number_of_players'] as int,
        playerPlaying = json['player_playing'] as int {
    List<dynamic> raw_tiles = json['tiles'];
    tiles = List.generate(
        raw_tiles.length, (index) => Tile.fromJSON(raw_tiles[index]));
  }

  Map<String, dynamic> toJson() => {
        'board_size': boardSize,
        'number_of_players': numberOfPlayers,
        'player_playing': playerPlaying,
        'tiles': tiles,
      };
}
