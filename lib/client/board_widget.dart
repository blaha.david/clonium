import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

import '../common/tile.dart';
import '../common/board.dart';
import 'board_painter.dart';
import 'client.dart';

class BoardWidget extends StatefulWidget {
  static const List<Color> playerColors = [
    Colors.black,
    Colors.red,
    Colors.green,
    Colors.blue,
    Colors.amber
  ];

  final String title;
  final int boardSize;
  final int numberOfPlayers;

  const BoardWidget(
      {Key? key,
      required this.title,
      required this.boardSize,
      required this.numberOfPlayers})
      : super(key: key);

  @override
  State<BoardWidget> createState() => _BoardWidgetState();
}

class _BoardWidgetState extends State<BoardWidget> {
  late Board _board;
  late Client client;
  bool loaded = false;

  final List<List<Tile>> _moves = [];
  int moveIndex = -1;

  @override
  void initState() {

    _board = Board(1,0);

    client = Client('94.142.235.163:9000');

    client.getBoard().then((b) {
      _board = b;
      print("got board!");
      loaded = true;
    });

    super.initState();
  }

  void onTapUp(TapUpDetails details) {
    int tileSize = MediaQuery.of(context).size.width ~/ _board.boardSize;
    double x = details.localPosition.dx;
    double y = details.localPosition.dy;
    int i = x ~/ tileSize;
    int j = y ~/ tileSize;

    if (!_board.validMove(i, j)) return;

    client.play(i, j).then((_) {
      client.getBoard().then((b) {
        setState(() {
          _board = b;
        });
      });
    });
  }

  List<Tile> _copyBoard() {
    return List.generate(
        _board.tiles.length, (index) => Tile.copy(_board.tiles[index]));
  }

  @override
  Widget build(BuildContext context) {
    double size = min(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
          backgroundColor: BoardWidget.playerColors[_board.playerPlaying],
        ),
        body: Center(
            child: GestureDetector(
          onTapUp: onTapUp,
          child: CustomPaint(
            size: Size(size, size),
            painter: BoardPainter(
              tiles: _board.tiles,
              size: widget.boardSize,
              tileSize: MediaQuery.of(context).size.width / widget.boardSize,
            ),
          ),
        )));
  }
}
