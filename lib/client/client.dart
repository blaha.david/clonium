import 'dart:convert';
import 'package:http/http.dart';

import '../common/tile.dart';
import '../common/board.dart';

class Client {
  final String endpoint;

  Client(this.endpoint);

  Future<Board> getBoard() async {
    var response = await get(Uri.http(endpoint, 'board'));
    if (response.statusCode == 200) {
      try {
         return Board.fromJSON(jsonDecode(response.body));
      } catch (e, _){
        return Future.error(e);
      }
    }
    return Future.error('Cannot GET /board -> ${response.statusCode}');
  }

  Future<bool> play(int x, int y) async {
    final data = {
      'x': x,
      'y': y,
    };
    final headers = {
      'Content-Type': 'application/json'
    };
    await post(Uri.http(endpoint, 'play'), headers: headers, body: json.encode(data));
    return true;
  }
}
