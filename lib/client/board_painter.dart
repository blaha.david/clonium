import 'dart:developer' as developer;
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import '../common/tile.dart';
import 'board_widget.dart';

class BoardPainter extends CustomPainter {
  final double gap = 2;
  final double dotGap = 10;
  final Paint painter = Paint()
    ..strokeWidth = 1
    ..style = PaintingStyle.fill;

  final int size;
  final double tileSize;
  final List<Tile> tiles;

  BoardPainter(
      {required this.size, required this.tileSize, required this.tiles});

  @override
  void paint(Canvas canvas, Size size) {
    tiles.asMap().forEach((index, tile) {
      painter.color = BoardWidget.playerColors[tile.player];
      canvas.drawRRect(getRect(index), painter);
      painter.color = Colors.white;
      for (int i = 0; i < tile.dots; i++) {
        canvas.drawRRect(getDot(index, i), painter);
      }
    });
  }

  RRect getRect(int index) {
    double left = (index % size) * tileSize;
    double top = (index ~/ size) * tileSize;
    double width = tileSize - gap;
    return RRect.fromRectAndRadius(
        Rect.fromLTWH(left, top, width, width), const Radius.circular(1.0));
  }

  RRect getDot(int index, int i) {
    double left = (index % size) * tileSize;
    double top = (index ~/ size) * tileSize;
    double width = tileSize - gap;
    double dotWidth = width / 2 - dotGap * 2;

    double dotLeft = left + (dotGap + dotWidth) * (i % 2 + 0.5);
    double dotTop = top + (dotGap + dotWidth) * (i ~/ 2 + 0.5);
    return RRect.fromRectAndRadius(
        Rect.fromLTWH(dotLeft, dotTop, dotWidth, dotWidth),
        const Radius.circular(50.0));
  }

  @override
  bool shouldRepaint(BoardPainter oldDelegate) => true;

  @override
  bool shouldRebuildSemantics(BoardPainter oldDelegate) => true;
}
