import 'package:flutter/material.dart';
import 'package:clonium/client/board_widget.dart';

void main() {
  runApp(const Clonium());
}

class Clonium extends StatelessWidget {
  const Clonium({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Clonium',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home:
          const BoardWidget(title: 'Clonium', boardSize: 8, numberOfPlayers: 2),
    );
  }
}
