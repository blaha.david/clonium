import 'dart:convert';

import '../common/board.dart';
import 'package:dchisel/dchisel.dart';

void main(List<String> args) {
  Server().run(args);
}

class Server {
  final headers = {
    'Content-type' : 'application/json',
  };
  Board? board;

  Future<void> run(List<String> args) async {
    DChisel().routeGet('/hello', (Request request) {
      return 'Hello world!';
    });

    DChisel().routeGet('/echo/<name>', (Request request, String name) {
      return 'Hello, $name';
    });

    DChisel().routePost('/post', (Request request) async {
      return 'POSTing';
    });

    DChisel().routePost('/newgame', (Request request) async {
      Map<String, dynamic> body;
      try {
        body = await request.body.asJson;
      } catch (e, _) {
        return Response(400, body: 'Bad request: ' + e.toString());
      }
      int boardSize = body['board_size'] ?? 8;
      int numberOfPlayers = body['number_of_players'] ?? 2;
      board = Board(boardSize, numberOfPlayers);

      return Response(201, body: 'Created');
    });

    DChisel().routeGet('/board', (Request request) async {
      if (board == null) {
        return Response(406, body: 'Not Acceptable: no board');
      }
      return Response(200, headers: headers, body: jsonEncode(board));
    });

    DChisel().routePost('/play', (Request request) async {
      if (board == null) {
        return Response(406, body: 'Not Acceptable: no board');
      }

      Map<String, dynamic> body;
      try {
        body = await request.body.asJson;
      } catch (e, _) {
        return Response(400, body: 'Bad request: ' + e.toString());
      }

      if (!body.containsKey('x') || !body.containsKey('y')) {
        return Response(400, body: 'Bad request: missing coordinates');
      }

      int x = body['x'];
      int y = body['y'];

      if (board!.validMove(x, y)) {
        board!.play(x, y);
        return Response(202, body: 'Accepted');
      } else {
        return Response(400, body: 'Bad request: invalid move');
      }
    });

    DChisel().serve(serverHost: '0.0.0.0', serverPort: 9000);
  }
}
